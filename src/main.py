#!/usr/bin/env python

from scanners import SynScanner, ConnectScanner, UdpScanner, FinScanner #@UnusedImport
import sys
from PyQt4.QtGui import QApplication
from gui import MainWindow
import logging

nonGuiTests = False
def nonGuiTestsFunction():
	scanner = SynScanner('127.0.0.1')
	portsStatus = scanner.scan([22, 52732, 80, 1000, 8000, 5000, 45691])
#	scanner = UdpScanner('127.0.0.1')
	scanner = FinScanner('192.168.1.39', '192.168.1.15')
#	portsStatus = scanner.scan([22, 52732, 80, 1000, 8000, 5000, 40695])
	portsStatus = scanner.scan([8000, 5000, 35147])
	print portsStatus



if __name__ == '__main__':
	logging.basicConfig(level = logging.DEBUG)

	if nonGuiTests:
		nonGuiTestsFunction()
		sys.exit()

	app = QApplication(sys.argv)

	mainWindow = MainWindow()

	mainWindow.show()
	sys.exit(app.exec_())
