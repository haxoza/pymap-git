import socket, dpkt, logging, netifaces #@UnresolvedImport

scanLogger = logging.getLogger('scanner')

class PortStatus(object):
	UNSCANNED = -1
	CLOSED = 0
	OPEN = 1
	UNKNOWN = 2

class Scanner(object):
	def __init__(self, remoteAddress, scanningInterface = 'lo', localPort = 0):
		self.remoteAddress = remoteAddress
		self.localAddress = netifaces.ifaddresses(scanningInterface)[netifaces.AF_INET][0]['addr']
		self.localPort = localPort
		self.portsStatus = None

	def scan(self, portsToCheck):
		self.stopScanning = False
		if type(portsToCheck) != list:
			portsToCheck = [portsToCheck]
		self.portsStatus = dict.fromkeys(portsToCheck, PortStatus.UNSCANNED)

		self.createSocket()

		for checked in portsToCheck:
			if self.stopScanning:
				self.closeSocket()
				return self.portsStatus
			self.portsStatus[checked] = self.scanOnePort(checked)

		self.closeSocket()
		return self.portsStatus

	def createSocket(self):
		raise NotImplemented

	def closeSocket(self):
		raise NotImplemented

	def scanOnePort(self, checked):
		raise NotImplemented

class TcpScanner(Scanner):
	def createPacket(self, port, flags):
		t = dpkt.tcp.TCP()
		t.seq = 0
		t.ack = 0
		t.win = 2048 #pewnie bez znaczenia
		t.flags = flags
		t.dport = port #ktory port skanuje
		t.sport = self.localPort #z jakiego portu skanuje
		# nmap dorzuca jeszcze do naglowka opcje 'mss 1460'

		# wyliczanie checksumy
		src = self.localAddress.split('.')
		dst = self.remoteAddress.split('.')
		src_packed = dpkt.struct.pack("BBBB", int(src[0]), int(src[1]), int(src[2]), int(src[3]))
		dst_packed = dpkt.struct.pack("BBBB", int(dst[0]), int(dst[1]), int(dst[2]), int(dst[3]))
		pseudo_header = dpkt.struct.pack('>4s4sxBH', src_packed, dst_packed, 6, len(str(t))) #http://en.wikipedia.org/wiki/Transmission_Control_Protocol#TCP_checksum_for_IPv4
		t.sum = 0 #wyzerowanie na czas obliczen
		t.sum = dpkt.in_cksum(pseudo_header + str(t)) #patrz wiki powyzej
		# koniec liczenia checksumy
		return t

	def checkResponse(self, tcp, host):
		if type(tcp) == dpkt.tcp.TCP and host == self.remoteAddress and tcp.dport == self.localPort:
			return True
		return False

class SynScanner(TcpScanner):
	def createSocket(self):
		self.scanSocket = socket.socket(socket.AF_INET, socket.SOCK_RAW, dpkt.ip.IP_PROTO_TCP)
		self.scanSocket.bind((self.localAddress, self.localPort))
		self.localPort = self.scanSocket.getsockname()[1]
		self.scanSocket.settimeout(0.7)

	def closeSocket(self):
		self.scanSocket.close()

	def scanOnePort(self, checked):
		t = self.createPacket(checked, dpkt.tcp.TH_SYN)
		self.scanSocket.sendto(str(t), (self.remoteAddress, checked)) # 2 i 3 argument nic nie daje
		scanLogger.info("Sent {0} to {1}:{2}".format(repr(t), self.remoteAddress, checked))

		packetCounter = 0
		while 1:
			try:
				# drugi pakiet to ten wlasciwy
				buf, addr = self.scanSocket.recvfrom(1024)
				h, unused = socket.getnameinfo(addr, 0)
				try:
					h = socket.gethostbyname(h)
				except socket.gaierror as error:
					scanLogger.warning("{0} : {1}".format(error, h))
				ip = dpkt.ip.IP(buf)
				t = ip.data

				packetCounter += 1
				scanLogger.info("Received packet #{0}: {1} from {2}".format(packetCounter, repr(t), h))

				if  self.checkResponse(t, h) and t.flags & dpkt.tcp.TH_ACK:
					if t.flags & dpkt.tcp.TH_SYN:
						return PortStatus.OPEN
					else:
						return PortStatus.CLOSED
			except socket.error:
				return PortStatus.UNKNOWN

class ConnectScanner(Scanner):
	def createSocket(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.bind((self.localAddress, self.localPort))
		self.localPort = self.socket.getsockname()[1]

	def closeSocket(self):
		self.socket.close()

	def scanOnePort(self, port):
		try:
			self.socket.connect((self.remoteAddress, port))
			self.socket.close()
			return PortStatus.OPEN
		except socket.error:
			return PortStatus.CLOSED

class UdpScanner(Scanner):
	def createSocket(self):
		self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, dpkt.ip.IP_PROTO_UDP)
		self.udp_socket.bind((self.localAddress, self.localPort))
		self.localPort = self.udp_socket.getsockname()[1]

		self.icmp_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, dpkt.ip.IP_PROTO_ICMP)
		# nie blokujaca opcja powoduje ze jak gniazdo puste to wyjatek
		self.icmp_socket.settimeout(2)

	def closeSocket(self):
		self.udp_socket.close()
		self.icmp_socket.close()

	def scanOnePort(self, port):
		u = dpkt.udp.UDP(dport = port, sport = self.localPort, data = 0)
		self.udp_socket.sendto(str(u), (self.remoteAddress, port)) # 2 i 3 argument nic nie daje
		scanLogger.info("Sent {0} to {1}:{2}".format(repr(u), self.remoteAddress, port))
		packetCounter = 0

		while 1:
			try:
				buf, addr = self.icmp_socket.recvfrom(1024)
				host, unused = socket.getnameinfo(addr, 0)
				try:
					host = socket.gethostbyname(host)
				except socket.gaierror as error:
					scanLogger.warning("{0} : {1}".format(error, host))
				ip = dpkt.ip.IP(buf)
				i = ip.data

				packetCounter += 1
				scanLogger.info("Received packet #{0}: {1} from {2}".format(packetCounter, repr(i), host))

				if host == self.remoteAddress and type(i.data) == dpkt.icmp.ICMP.Unreach:
					return PortStatus.CLOSED
			except socket.error:
				return PortStatus.UNKNOWN

class FinScanner(TcpScanner):
	def createSocket(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, dpkt.ip.IP_PROTO_TCP)
		self.socket.bind((self.localAddress, self.localPort))
		self.localPort = self.socket.getsockname()[1]
		self.socket.settimeout(0.7)

	def closeSocket(self):
		self.socket.close()

	def scanOnePort(self, port):
		t = self.createPacket(port, dpkt.tcp.TH_FIN)
		self.socket.sendto(str(t), (self.remoteAddress, port)) # 2 i 3 argument nic nie daje
		scanLogger.info("Sent {0} to {1}:{2}".format(repr(t), self.remoteAddress, port))
		packetCounter = 0

		while 1:
			try:
				buf, addr = self.socket.recvfrom(0xffff)
				h, unused = socket.getnameinfo(addr, 0)
				try:
					h = socket.gethostbyname(h)
				except socket.gaierror as error:
					scanLogger.warning("{0} : {1}".format(error, h))
				ip = dpkt.ip.IP(buf)
				t = ip.data

				packetCounter += 1
				scanLogger.info("Received packet #{0}: {1} from {2}".format(packetCounter, repr(t), h))

				if self.checkResponse(t, h) and t.flags & dpkt.tcp.TH_RST and t.flags & dpkt.tcp.TH_ACK:
					return PortStatus.CLOSED
			except socket.error:
				return PortStatus.UNKNOWN
