from threading import Thread
import copy

class ScanJob(object):
	def __init__(self, scanner, portsToScan):
		self.scanner = scanner
		self.portsToScan = portsToScan
		self.lastResults = None
		self._scanThread = None
		
	def start(self):
		self._scanThread = Thread(target=self.scanner.scan, kwargs={"portsToCheck" : self.portsToScan})
		self._scanThread.start()
	
	def stop(self):
		self.scanner.stopScanning = True
	
	def isFinished(self):
		return not self._scanThread.isAlive()

	def getAllResults(self):
		return copy.deepcopy(self.scanner.portsStatus)
	
	def giveNewResults(self):
		allPortStates = self.getAllResults()
		if not self.lastResults:
			self.lastResults = allPortStates
			return allPortStates
		
		newResults = self._getChanges(new=allPortStates, old=self.lastResults)
		self.lastResults = allPortStates
		return newResults
	
	def _getChanges(self, new, old):
		changes = {}
		for (key, val) in new.items():
			if old[key] != val:
				changes[key] = val
		return changes

class ServicesBook(object):
	UDP, TCP = range(2)
	
	def __init__(self):
		self._portsToServices = {}
		self._portPopularity = {}
		self._tcpPortPopularityRank = []
		self._udpPortPopularityRank = []
		self._importThread = Thread(target=self)
		self._importThread.start()
	
	def getMostPopularPorts(self, howMany=10, protocol=TCP):
		rankList = self._tcpPortPopularityRank
		if protocol == ServicesBook.UDP:
			rankList = self._udpPortPopularityRank
		return rankList[:howMany]
	
	def getServiceName(self, port, protocol):
		return self._portsToServices.get((port, protocol))
	
	def __call__(self):
		self._importDict()
		
	def _importDict(self):
		for line in open("nmap-services"):
			if line[0] == '#':
				continue
			(service, port, protocol, frequency) = ServicesBook._parseOneRow(line)
			if protocol == None:
				continue
			self._portsToServices[(port, protocol)] = service
			self._portPopularity[(port, protocol)] = frequency
		
		self._initPopularityRanks()
	
	@classmethod
	def _parseOneRow(ServicesBook, line):
			row = line.split('\t')
			service = row[0]
			if service == 'unknown':
				service = None
			
			port_s, protocol_s = row[1].split('/')
			protocol = None
			if protocol_s == 'udp':
				protocol = ServicesBook.UDP
			elif protocol_s == 'tcp':
				protocol = ServicesBook.TCP
			
			return (service, int(port_s), protocol, float(row[2]))
		
	def _initPopularityRanks(self):
		portsRank = self._portPopularity.keys()
		portsRank.sort(key = lambda k: self._portPopularity[k], reverse=True)
		
		for entry in portsRank:
			if entry[1] == ServicesBook.TCP:
				self._tcpPortPopularityRank.append(entry[0])
			else:
				self._udpPortPopularityRank.append(entry[0])
				