#-*- coding: utf-8 -*-
from PyQt4.Qt import Qt
from PyQt4.QtGui import QMainWindow, QComboBox, QFileDialog, QTableWidgetItem
from PyQt4.QtCore import QTimer
import re
import logging
from scanners import SynScanner, ConnectScanner, UdpScanner, PortStatus,\
	FinScanner
from commons import ServicesBook, ScanJob
import time, netifaces #@UnresolvedImport

guiLogger = logging.getLogger("guiLogger")
howManyPopularPorts = 10

class MainWindow(QMainWindow):
	def __init__(self):
		from ui.ui_mainWindow import Ui_MainWindow
		QMainWindow.__init__(self)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.portsToScanParser = PortsInputParser()
		self.addScanners()
		self.addNetworkInterfaces()
		self.servicesBook = ServicesBook()
		self.informants = [LogInformant(self.ui.resultsEdit, self.servicesBook), TableInformant(self.ui.portTable, self.servicesBook)]
		self.scanJob = None
		self.scanResultsUpdater = QTimer()

	def addScanners(self):
		self.scannersToTextRepr = {UdpScanner: "Skanowanie UDP", FinScanner: "Skanowanie Fin", SynScanner: "Skanowanie SYN", ConnectScanner: "Skanowanie Connect"}
		for scanner, text in self.scannersToTextRepr.iteritems():
			self.ui.scanTypeComboBox.addScanType(scanner, text)
	
	def addNetworkInterfaces(self):
		map(lambda iface: self.ui.ifaceComboBox.addItem(iface), filter(lambda iface: netifaces.ifaddresses(iface).has_key(netifaces.AF_INET), netifaces.interfaces()))

	def scan(self):
		try:
			portsToScan = self.getPortsToScan()
		except SyntaxError:
			guiLogger.info("Błąd parsowania portów do skanowania.")
			return

		scanningInterface = str(self.ui.ifaceComboBox.currentText())
		scanner = self.ui.scanTypeComboBox.getCurrentScannerClass()(self.getAddressToScan(), scanningInterface)
		guiLogger.info("Skanuję porty {0} za pomocą {1}.".format(portsToScan, type(scanner).__name__))
		for informant in self.informants:
			informant.infoScanStart(len(portsToScan), self.scannersToTextRepr[type(scanner)])

		self.scanJob = ScanJob(scanner, portsToScan)
		self.scanJob.start()
		self.scanResultsUpdater.timeout.connect(self.updateScanResults)
		self.scanResultsUpdater.start(1000)
	
	def updateScanResults(self):
		results = self.scanJob.giveNewResults()
		for informant in self.informants:
			informant.infoScanResults(results, self.getScanProtocol())
		if self.scanJob.isFinished():
			self.scanResultsUpdater.stop()
	
	def getScanProtocol(self):
		if self.ui.scanTypeComboBox.getCurrentScannerClass() == UdpScanner:
			return ServicesBook.UDP
		return ServicesBook.TCP
	
	def getAddressToScan(self):
		input = self.ui.adressEdit.text()
		if input == "":
			input = "127.0.0.1"
		return input
	
	def getPortsToScan(self):
		if self.ui.popularServicesOnlyCheckbox.isChecked():
			if self.getScanProtocol() == ServicesBook.UDP:
				return self.servicesBook.getMostPopularPorts(howManyPopularPorts, ServicesBook.UDP)
			return self.servicesBook.getMostPopularPorts(howManyPopularPorts, ServicesBook.TCP)
			
		return self.portsToScanParser.parse(str(self.ui.portsEdit.text()))
		
	def saveLog(self):
		saveFile = QFileDialog.getSaveFileName(parent=self, caption=self.trUtf8("Wybierz plik do zapisu logów."))
		if not saveFile:
			return
		
		with open(saveFile, "w") as f:
			f.write(self.ui.resultsEdit.toPlainText())
	
	def togglePopularServicesOnly(self, checked):
		self.ui.portsEdit.setEnabled(not checked)
		
	def closeEvent(self, event):
		if self.scanJob:
			self.scanJob.stop()
		QMainWindow.closeEvent(self, event)

class Informant(object):
	def __init__(self, logObject, serviceBook):
		self.logObject = logObject
		self.serviceBook = serviceBook
		self.portResultText = {PortStatus.UNSCANNED: "nie skanowano", PortStatus.UNKNOWN: "nie rozpoznano", PortStatus.OPEN: "otwarty", PortStatus.CLOSED: "zamknięty"}

	def getPortTextWithServiceName(self, port, protocol):
		portText = str(port)
		serviceName = self.serviceBook.getServiceName(port, protocol)
		if serviceName:
			portText = "{0} [{1}]".format(portText, serviceName)
		return portText
	
class LogInformant(Informant):
	def __init__(self, logObject, serviceBook):
		Informant.__init__(self, logObject, serviceBook)
	
	def infoScanStart(self, portsCount, scanMethod):
		self.appendLine("Skanowanie {0} portów metodą {1}...".format(str(portsCount), scanMethod))
	
	def infoScanResults(self, portsStatus, protocol):
		for port, result in portsStatus.iteritems():
			if result != PortStatus.UNSCANNED:
				self.infoOneScanResult(port, result, protocol)
	
	def infoOneScanResult(self, port, result, protocol):
		self.appendLine("{0} : {1}".format(self.getPortTextWithServiceName(port, protocol), self.portResultText[result]))
	
	def appendLine(self, line):
		self.logObject.append(self.logObject.trUtf8("{0} {1}".format(self.getTimeStamp(), line)))
	
	def getTimeStamp(self):
		return time.strftime("[%H:%M:%S]")

class TableInformant(Informant):
	def __init__(self, logObject, serviceBook):
		Informant.__init__(self, logObject, serviceBook)
		self.portResultColor = {PortStatus.UNSCANNED: None, PortStatus.UNKNOWN: Qt.yellow, PortStatus.OPEN: Qt.green, PortStatus.CLOSED: Qt.red}
		
	def infoScanStart(self, portsCount, scanMethod):
		self.logObject.clearContents()
		self.logObject.setRowCount(portsCount)
		
	def infoScanResults(self, portsStatus, protocol):
		items = portsStatus.items()
		items.sort()
		row = 0
		for port, result in items:
			self.infoOneScanResult(row, port, result, protocol)
			row += 1

	def infoOneScanResult(self, row, port, result, protocol):
		portItem = QTableWidgetItem(self.logObject.trUtf8(self.getPortTextWithServiceName(port, protocol)))
		statusItem = QTableWidgetItem(self.logObject.trUtf8(str(self.portResultText[result])))
		if self.portResultColor[result]:
			portItem.setBackgroundColor(self.portResultColor[result])
			statusItem.setBackgroundColor(self.portResultColor[result])
		self.logObject.setItem(row, 0, portItem)
		self.logObject.setItem(row, 1, statusItem)


class ScanTypeComboBox(QComboBox):
	def __init__(self, parent = None):
		QComboBox.__init__(self, parent)
		self.scanners = {PortStatus.UNSCANNED: "nie skanowano", PortStatus.UNKNOWN: "nie rozpoznano", PortStatus.OPEN: "otwarty", PortStatus.CLOSED: "zamknięty"}

	def addScanType(self, scanClass, scanText):
		self.scanners[self.count()] = scanClass
		self.insertItem(self.count(), scanText)

	def getCurrentScannerClass(self):
		return self.scanners[self.currentIndex()]

class PortsInputParser(object):
	def __init__(self):
		self.singlePort = re.compile(r"^\s*(\d+)\s*$")
		self.rangeOfPorts = re.compile(r"^\s*(\d+)-(\d+)\s*$")

	def parse(self, string):
		if string == "":
			return range(1, 65536)

		ports = set()
		for elem in string.split(","):
			result = self.parseSingleElement(elem)
			if type(result) == list:
				ports.update(result)
			else:
				ports.add(result)

		return list(ports)

	def parseSingleElement(self, elem):
		matchResult = self.singlePort.match(elem)
		if matchResult == None:
			matchResult = self.rangeOfPorts.match(elem)
			if matchResult == None:
				raise SyntaxError
			else:
				return range(int(matchResult.group(1)), int(matchResult.group(2)) + 1)
		else:
			return int(matchResult.group(1))
