Uruchomienie programu:

$ cd src/
$ make				# budowa interfejsu graficznego
$ su -				# uzyskanie praw administratora systemu
# ./main.py			# uruchomienie glownego pliku

Niezbędne biblioteki:

* dpkt
* PyQt4
* netifaces
