import unittest
from gui import PortsInputParser


class PortsInputParserTest(unittest.TestCase):

	def testCommaSeparatedPorts(self):
		#given
		parser = PortsInputParser()
		string = "53, 64, 1004"
		#when
		ports = parser.parse(string)
		#then
		self.assertEquals(set(ports), set([53, 64, 1004]))
		
	def testRangePorts(self):
		#given
		parser = PortsInputParser()
		string = "53-55, 77-77"
		#when
		ports = parser.parse(string)
		#then
		self.assertEquals(set(ports), set([53, 54, 55, 77]))
		
	def testRaisesErrorOnBadInput(self):
		#given
		parser = PortsInputParser()
		string = "53-55,"
		#when, then
		self.assertRaises(SyntaxError, parser.parse, string)
		
	def testGivesAllPortsOnEmpty(self):
		#given
		parser = PortsInputParser()
		string = ""
		#when
		ports = parser.parse(string)
		#then
		self.assertEquals(set(ports), set(range(1,65536)))
		
	def testHandlesStrangeInputs(self):
		#given
		parser = PortsInputParser()
		string = "54, 53-55, 77-33"
		#when
		ports = parser.parse(string)
		#then
		self.assertEquals(set(ports), set([53, 54, 55]))

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()