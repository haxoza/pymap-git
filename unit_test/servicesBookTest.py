import unittest
from commons import ServicesBook


class ServicesBookTest(unittest.TestCase):

	def testParseOneRow(self):
		#given
		row = "tcpmux	1/tcp	0.001995	# TCP Port Service Multiplexer [rfc-1078]"
		#when
		result = ServicesBook._parseOneRow(row)
		#then
		self.assertEquals(result, ("tcpmux", 1, ServicesBook.TCP, 0.001995))
		
	def testParseOneRowWithUnknownService(self):
		#given
		row = "unknown	4/udp	0.000477"
		#when
		result = ServicesBook._parseOneRow(row)
		#then
		self.assertEquals(result, (None, 4, ServicesBook.UDP, 0.000477))

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()